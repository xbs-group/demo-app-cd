#######
# Helm
#######
h-install:
	helm install demo-app ./app-chart --values app-env/dev/dev-values.yaml -n argocd

h-upgrade:
	helm upgrade demo-app ./app-chart --values app-env/dev/dev-values.yaml -n argocd

h-uninstall:
	helm uninstall demo-app -n argocd


#########
# ArgoCD
#########

a-install:
	kubectl apply -f argocd/values-dev.yaml

a-uninstall:
	argocd app delete argocd/demo-app-dev
