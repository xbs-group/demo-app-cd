# Deployment of ait-demo-app
CD configuration for the app from [ait-demo-app](https://github.com/propalparolnapervom/ait-demo-app) repo

## Usage

### Option 1: By Helm

Install the helm chart
```
make h-install
```

Upgrade the helm chart
```
make h-upgrade
```

Uninstall the helm chart
```
make h-uninstall
```


### Option 2: By ArgoCD

Install the helm chart
```
make a-install
```

Delete the helm chart
```
make a-uninstall
```


